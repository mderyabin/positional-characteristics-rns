--
--
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.compare_pkg.all;

entity compare is
	Port (
		X      : in  rns_number(0 to N-1);
		Y      : in  rns_number(0 to N-1);
		output : out std_logic_vector (2 downto 0)
	);
end compare;

architecture Behavioral of compare is
	component d	
	port (
		X      : in  rns_number;          
		output : out unsigned(pBC-1 downto 0)
	);
	end component;
	
	signal dx : unsigned(pBC-1 downto 0);
	signal dy : unsigned(pBC-1 downto 0);
begin

	inst_d_x: d  
	port map (
		X      => X,
		output => dx
	);
	
	inst_d_y: d  
	port map (
		X      => Y,
		output => dy
	);
	
	output <= "100" when dx > dy else
				 "001" when dx < dy else
				 "010";

end Behavioral;

