-- moduli set: {107, 109, 113, 121, 125, 126, 127}

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package compare_pkg is

	-- moduli's count
	constant N : natural := 9;
	
	constant rnsBC    : natural := 5;	-- moduli bit's count
	constant pBC    : natural := 28;		-- range bit's count
	
	-- bit's count for pairing circuit
	constant pairBC : natural := pBC;
	
	-- special type for multiply input in entity
	type rns_number is array (natural range <>) of unsigned(rnsBC-1 downto 0);
	
	-- RNS range
	constant P : unsigned (pBC-1 downto 0) := "1101010011000010000010000110";
	
	-- constants
	type b_array is array (0 to N-1) of unsigned(pBC - 1 downto 0);
	constant b : b_array := (
		0 => "0110101001100001000001000011",
		1 => "1000110111010110101100000100",
		2 => "1010101000110100110100111000",
        3 => "0101101100101110100101011110",
		4 => "1000011101100100001100111110",
		5 => "0111001010001111110111010010",
        6 => "1010111100110110011000010100",
        7 => "1001110011000100110111011100",
        8 => "1011100100000001110001001000"
	);
	
--	-- moduli set for testbench
--	type m_array is array (0 to N-1) of unsigned(rnsBC downto 0);
--	constant m : m_array := (
--		"010",
--		"011",
--		"101",
--		"111");

end compare_pkg;

package body compare_pkg is

 
end compare_pkg;
