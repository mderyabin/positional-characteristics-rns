library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- calculate c = k*a mod p
entity constmult_mod is
	Generic (
		constant p  : unsigned := "11110111111101111111011111110111";	-- modulus
		constant k  : unsigned := "00110111111101111111011111110111";	-- constant, k'length = p'length
		constant BC : natural  := 16;
		constant B  : natural  := 16												-- size of block's
	);
	Port (
		a : in  unsigned(BC - 1 downto 0);			-- non-constant input
		c : out unsigned(p'length - 1 downto 0)	-- = a * k mod p
	);
end constmult_mod;

architecture Behavioral of constmult_mod is

	-- components declaration ----
	COMPONENT Add_mod
	GENERIC (
		p : unsigned
	);
	PORT(
		A : IN  unsigned ;
		B : IN  unsigned ;          
		Sum_mod : OUT unsigned 
	);
	END COMPONENT;
	------------------------------
		
	-- pairint functions declaration ----
	-- ���������� ��������� �� ����� ���� � ������� layer_num
	function LayerInputSize (layer_num: natural; digit_count: natural) return natural is
		variable result : natural;
	begin
		result := digit_count;
		
		if layer_num < 2 then
			return result;
		end if;
		
		for i in 2 to layer_num loop
			result := (result / 2) + (result mod 2);
		end loop;
		return result;
	end LayerInputSize;
	
	-- ��������� ������� ��������� ���� layer_num � � ����� ������� ��������
	function LayerStart (layer_num: natural; digit_count: natural) return natural is
		variable result : natural;
	begin
		result := 0;
		for i in 1 to layer_num-1 loop
			result := result + LayerInputSize(i, digit_count);
		end loop;
		return result;
	end LayerStart;
	
	-- ���������� ����� - ����������� �������� �� ���������� ��������� ������� ���� (���������)
	-- � ����������� �����
	function LayersCount(digit_count: in natural) return natural is
		variable res : natural;
		variable vn  : unsigned(31 downto 0);	-- �����, ����������� ������� ���� natural
	begin
		res := 0;
		
		vn := to_unsigned(digit_count-1, 32);		-- digit_count-1 ����� ����� ������� log(2^t)
		
		-- ������� ���������� ��� � ����� vn
		while to_integer(vn) /= 0 loop
			res := res + 1;
			vn := vn srl 1;
		end loop;
		
		return res;
	end function LayersCount;
	
	-- ����� ���������� ��������� �� ���� �����, ������� ��������
	function AllLayersSize(layers_count, digit_count : in natural) return natural is
		variable res : natural;
	begin
		res := 0;
		
		-- ������������ ���������� �������� �� ���� ����� ���� �������� �������
		for ln in 1 to layers_count+1 loop
			res := res + LayerInputSize(ln, digit_count);
		end loop;
		
		return res;
	end function AllLayersSize;
	------------------------------	
	
	-- division and mod functions ---
	-- this internal procedure computes UNSIGNED division
  -- giving the quotient and remainder.
  procedure myDIVMOD (NUM, XDENOM: UNSIGNED; XREMAIN: out UNSIGNED) is
    variable TEMP: UNSIGNED(NUM'LENGTH downto 0);
--    variable QUOT: UNSIGNED(MAX(NUM'LENGTH, XDENOM'LENGTH)-1 downto 0);
    alias DENOM: UNSIGNED(XDENOM'LENGTH-1 downto 0) is XDENOM;
    variable TOPBIT: INTEGER;
  begin
    TEMP := "0"&NUM;
--    QUOT := (others => '0');
    TOPBIT := -1;
    for J in DENOM'RANGE loop
      if DENOM(J)='1' then
        TOPBIT := J;
        exit;
      end if;
    end loop;
    --assert TOPBIT >= 0 report "DIV, MOD, or REM by zero" severity ERROR;

    for J in NUM'LENGTH-(TOPBIT+1) downto 0 loop
      if TEMP(TOPBIT+J+1 downto J) >= "0"&DENOM(TOPBIT downto 0) then
        TEMP(TOPBIT+J+1 downto J) := (TEMP(TOPBIT+J+1 downto J))
            -("0"&DENOM(TOPBIT downto 0));
--        QUOT(J) := '1';
      end if;
--      assert TEMP(TOPBIT+J+1)='0'
--          report "internal error in the division algorithm"
--          severity ERROR;
    end loop;
    --XQUOT := RESIZE(QUOT, XQUOT'LENGTH);
    XREMAIN := RESIZE(TEMP, XREMAIN'LENGTH);
  end myDIVMOD;
	---------------------------------
	
	-- constants declaration -----
	constant p_bc 			: natural := p'length;
	constant k_bc        : natural := p_bc;
	--constant BC 			: natural := p_bc;
	constant pow2_B      : natural := to_integer(SHIFT_LEFT(to_unsigned(1, B+1), B));
	 
	constant bl_count    : natural := BC / B;
	constant add_B       : natural := BC - B * bl_count;
	
	constant pow2_add_B  : natural := to_integer(SHIFT_LEFT(to_unsigned(1, add_B+1), add_B));
	
	constant l_count     : natural := LayersCount(bl_count);					-- ���������� �����
	
	constant all_l_count : natural := AllLayersSize(l_count, bl_count);	-- ���������� ��������� �� ���� �����
	------------------------------
		
	-- types declaration ---------
	type RAM_type is array (0 to pow2_B - 1) of unsigned (p_bc - 1 downto 0);
	type RAM_table_type is array (0 to bl_count - 1) of RAM_type;
	
	type add_RAM_type is array (0 to pow2_add_B - 1) of unsigned (p_bc - 1 downto 0);
	
	type bl_type is array (0 to all_l_count - 1) of unsigned (p_bc - 1 downto 0);
	------------------------------
	
	-- functions declaration -----
--	function min (x: natural; y : natural) return natural is
--	begin
--		if (x > y) then return y; end if;
--		return x;
--	end min;
	
	function CreateMultRAM(j : in natural) return RAM_type is
		variable result : RAM_type;
		variable temp : unsigned(BC + k_bc - 1 downto 0);
--		variable t : natural;
--		variable step : natural := 1;
	begin
		for i in 0 to pow2_B - 1 loop 
--			result(i) := to_unsigned(i, B) * k mod p;
--			t := 1;
--			while t <= j * B loop
--				if t > j * B then
--					step := t - j * B;
--				end if;
--				result(i) := SHIFT_LEFT(resize(result(i), step + p_bc), step) mod p;
--				t := t + step;
--			end loop;
		
			--result(i) := SHIFT_LEFT(resize((to_unsigned(i, B) * k), BC + k_bc), j * B) mod p;
			temp := SHIFT_LEFT(resize((to_unsigned(i, B) * k), BC + k_bc), j * B);
			myDIVMOD(temp, p, result(i));
			
		end loop;
		return result;
	end CreateMultRAM;
	
	function CreateMultRAMTable return RAM_table_type is
		variable result : RAM_table_type;
	begin
		for j in 0 to bl_count-1 loop
			result(j) := CreateMultRAM(j);
		end loop;
		return result;
	end CreateMultRAMTable;
	
	function CreateMultAddRAM return add_RAM_type is
		variable result : add_RAM_type;
		variable temp : unsigned(BC + k_bc - 1 downto 0);
--		variable t : natural;
--		variable step : natural := 1;
	begin
		if add_B /= 0 then
			for i in 0 to pow2_add_B - 1 loop 
--				result(i) := to_unsigned(i, add_B) * k mod p;
--				t := 1;
--				while t <= bl_count * B loop
--					if t > bl_count * B then
--						step := t - bl_count * B;
--					end if;
--					result(i) := SHIFT_LEFT(resize(result(i), step + p_bc), step) mod p;
--					t := t + step;
--				end loop;

--				result(i) := SHIFT_LEFT(resize((to_unsigned(i, add_B) * k), BC + k_bc), bl_count * B) mod p;
				
				temp := SHIFT_LEFT(resize((to_unsigned(i, add_B) * k), BC + k_bc), bl_count * B);
				myDIVMOD(temp, p, result(i));
				
			end loop;
		else
			for i in 0 to pow2_add_B - 1 loop 
				result(i) := (others => '0');
			end loop;
		end if;
		return result;
	end CreateMultAddRAM;
	
	
	------------------------------

	-- RAM declaration -----------
	constant mult_RAM_table : RAM_table_type := CreateMultRAMTable;
	constant mult_add_RAM : add_RAM_type := CreateMultAddRAM;
	------------------------------
	
	-- signals declaration -------
	signal blocks     : bl_type;
	signal add_block  : unsigned (p_bc - 1 downto 0);
	
	signal last_block : unsigned (p_bc - 1 downto 0) := (others => '0');
	------------------------------
	
begin

	level_1: for i in 0 to bl_count-1 generate
	begin
		--blocks(i) <= SHIFT_LEFT(resize(mult_RAM(to_integer(a(B*(i+1)-1 downto B*i))), BC + k_bc), i * B); 
		blocks(i) <= mult_RAM_table(i)(to_integer(a(B*(i+1)-1 downto B*i)));
	end generate;
	
	if0 : if bl_count /= 0 generate
		layers: for i in 1 to l_count generate
		begin
			
			-- ������� 2: �������� ��������� �������� �����������
			level_2: for j in 0 to LayerInputSize(i, bl_count) / 2 - 1  generate
			begin
				--blocks(LayerStart(i + 1, bl_count) + j) <= blocks(LayerStart(i, bl_count) + j) + blocks(LayerStart(i, bl_count) + LayerInputSize(i, bl_count) - 1 - j);
				Add_mod_inst: Add_mod 
				GENERIC MAP (
					p => p
				)
				PORT MAP(
					A => blocks(LayerStart(i, bl_count) + j),
					B => blocks(LayerStart(i+1, bl_count) - 1 - j),
					Sum_mod => blocks(LayerStart(i + 1, bl_count) + j)
				);
			end generate level_2;
			
			-- ������� ������������ �������� � ������ ��������� ����� �������� �� ����
			if_odd: if ((LayerInputSize(i, bl_count) mod 2) = 1) generate
				blocks(LayerStart(i+2, bl_count) - 1) <= blocks(LayerStart(i, bl_count) + LayerInputSize(i, bl_count)/2);
			end generate if_odd;
			
		end generate layers;
		
		last_block <= blocks(all_l_count - 1);
	end generate if0;
	
	if1 : if bl_count = 0 generate
		last_block <= (others => '0');
	end generate if1;
	
	if_over: if add_B /= 0 generate
		--add_block <= SHIFT_LEFT(resize(mult_RAM(to_integer(a(BC-1 downto bl_count * B))), BC + k_bc), bl_count * B);
		add_block <= mult_add_RAM(to_integer(a(BC-1 downto bl_count * B)));
		
		--c <= blocks(all_l_count - 1) + add_block;
		Add_mod_inst: Add_mod 
			GENERIC MAP (
				p => p
			)
			PORT MAP(
				A => last_block,
				B => add_block,
				Sum_mod => c
			);
	end generate if_over;
	
	if_n_over: if add_B = 0 generate
		c <= blocks(all_l_count - 1);
	end generate if_n_over;

end Behavioral;

