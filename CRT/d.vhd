--
--
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.compare_pkg.all;
use work.pairing_mod_package.all;

entity d is
	Port (
		X      : in  rns_number(0 to N-1);
		output : out unsigned (pBC-1 downto 0)
	);
end d;

architecture Behavioral of d is
	COMPONENT pairing_mod
	GENERIC (
		constant p: unsigned;
		constant N: NATURAL
	);
	PORT(
		input : IN input_block;          
		output : OUT unsigned(pairBC-1 downto 0)
	);
	END COMPONENT;

	COMPONENT constmult_mod
	Generic (
		constant p  : unsigned;
		constant k  : unsigned;
		constant BC : natural;
		constant B  : natural
	);
	Port (
		a : in  unsigned;
		c : out unsigned
	);
	END COMPONENT;
	
	signal bx : input_block(0 to N-1);
	signal q  : unsigned(pairBC - 1 downto 0);
begin

	for1 : for I in 0 to N-1 generate
		inst_constmult_mod: constmult_mod
		generic map (
			p  => P,
			k  => b(I),
			BC => rnsBC,
			B  => 5
		)
		port map (
			a => X(I),
			c => bx(I)
		);
	end generate for1;
	
	inst_pairing: pairing_mod
	GENERIC MAP (
		p => P,
		N => N
	)
	PORT MAP(
		input => bx,
		output => output
	);

end Behavioral;

