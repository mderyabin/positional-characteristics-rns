-- 
-- ���������, ������������� ����� ��������� ������� ������� ����������
-- �����: ������ �������
-- ����: 24.03.2015
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.pairing_mod_package.all;

entity pairing_mod is
	Generic (
		constant p : unsigned(BC - 1 downto 0);
		constant N : natural := CN								-- ������ �������� �������
	);
	Port (
		input  : in  input_block(0 to N-1);
		output : out unsigned(BC-1 downto 0)
	);
end pairing_mod;

architecture Behavioral of pairing_mod is

	-- ����������� ����������, ����������� �� ���� ���� ����������
	COMPONENT pairing_mod_layer
		GENERIC (
			constant p : unsigned;
			constant N : NATURAL
		);	
		PORT(
			input : IN  input_block;
			output : OUT  input_block
		);
   END COMPONENT;
	
	
	---------------------------------------------------------------
	-- ���� ������� ��� ������� �������� �������� 
	
	-- ��������� ���������� ��������� � ���� �� ��� ������
	-- ����������� ������ �� ������� ����
	function getLayerSize(ln, n : in natural) return natural is
		variable res : natural;
	begin
		res := n;
		
		for i in 2 to ln loop
			-- ���������� ��������� ���� ����� �������� ���������� ��������� ����������� ���� 
			-- ���� 1 � ������, ���� ���������� ���� �������� �������� ���������� ���������
			res := res/2 + (res mod 2);	
		end loop;
		
		return res;
	end function getLayerSize;
	
	-- ��������� ��������� ������� ����
	function getLayerStart(ln, n : in natural) return natural is
		variable res : natural;
	begin
		res := 0;
		
		-- ��������� ������� ���� ����� ������ �� ���� �� ���������� ���� ��������� ���������� �����
		for i in 1 to ln-1 loop
			res := res + getLayerSize(i, n);
		end loop;
		
		return res;
	end function getLayerStart;
	
	-- ��������� ���������� ���� ����������� �� ���� ������
	function getAllLayersSize(lc, n : in natural) return natural is
		variable res : natural;
	begin
		res := 0;
		
		-- ������������ ���������� �������� �� ���� ����� ���� �������� �������
		for ln in 1 to lc+1 loop
			res := res + getLayerSize(ln, n);
		end loop;
		
		return res;
	end function getAllLayersSize;
	
	-- ��������� ���������� �����
	-- �������� �������� ����� N � ����������� �����
	function getLayersCount(n: in natural) return natural is
		variable res : natural;
		variable vn : unsigned(31 downto 0);	-- �����, ����������� ������� ���� natural
	begin
		res := 0;
		
		vn := to_unsigned(n-1, 32);		-- n-1 ����� ����� ������� log(2^t)
		
		-- ������� ���������� ��� � ����� vn
		while to_integer(vn) /= 0 loop
			res := res + 1;
			vn := vn srl 1;
		end loop;
		
		return res;
	end function getLayersCount;
	---------------------------------------------------------------
	
	constant layers_count : natural := getLayersCount(N);							-- ���������� �����
	
	constant signal_size : natural := getAllLayersSize(layers_count, n);		-- ������ ������� ��������
	signal signals: input_block (0 to signal_size-1);								-- ������ ��������:
																										-- ������ ��� �������, ����������� ����
	
begin

	-- ������ ������� ������ � ������ ��������
	f1 : for I in 0 to N-1 generate
		signals(I) <= input(I);
	end generate f1;
	
	-- ��������� �����
	f2 : for I in 1 to layers_count generate
	
		-- ����������� ���� ����������
		-- �� ���� �������� ��������� ������� ����������� ����
		layer : pairing_mod_layer
		GENERIC MAP ( 
			p => p,
			N => getLayerSize(I, N) 
		)
		PORT MAP (
			 input => signals(getLayerStart(I, N) to getLayerStart(I, N)+getLayerSize(I, N)-1),
			 output => signals(getLayerStart(I+1, N) to getLayerStart(I+1, N)+getLayerSize(I+1, N)-1)
		);
	end generate f2;
	
	-- ��������� ������ ���������� ���� �������� �� �����
	output <= signals(signal_size-1);

end Behavioral;

