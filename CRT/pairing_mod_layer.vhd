-- 
-- ���������, ���������� �� ���� ���� ����������
-- �����: ������ �������
-- ����: 24.03.2015
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


use work.pairing_mod_package.all;

entity pairing_mod_layer is
	Generic (
		constant p: unsigned(BC - 1 downto 0);
		constant N: natural := 8										-- ���������� ��������� ����
	);
	Port (
		input  : in  input_block(0 to N-1);							-- ����: ������ ������� N
		output : out input_block(0 to N/2 - 1 + (N mod 2) )	-- �����: ������ ������� N/2 (���� 1 � ������, ���� N �������)
	);
end pairing_mod_layer;

architecture Behavioral of pairing_mod_layer is
	COMPONENT Add_mod
	GENERIC (
		p : unsigned
	);
	PORT(
		A : IN  unsigned;
		B : IN  unsigned;          
		Sum_mod : OUT unsigned
	);
	END COMPONENT;
begin
	
	-- ��������� ������ �������� ��� ��������� �������� �������
	-- ����������� �������� �� ������ � ���������� � �����
	for1: for I in 0 to (N / 2 - 1) generate
		Add_mod_inst: Add_mod 
		GENERIC MAP (
			p => p
		)
		PORT MAP(
			A => input(I),
			B => input(N-I-1),
			Sum_mod => output(I)
		);
	end generate for1;
	
	-- ����� �������� �������� �������� � ������, ���� N �������
	if1: if (N mod 2 = 1) generate 
		output(N / 2) <= input(N / 2);
	end generate if1;

end Behavioral;

