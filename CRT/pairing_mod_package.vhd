-- 
-- ����� ��� ����������� ����������� ������ ��� ������ ������������ ����������
-- �����: ������ �������
-- ����: 24.03.2015
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.compare_pkg.all;

package pairing_mod_package is
	
	constant BC : natural := pairBC;
	constant CN : natural := 9;

	type input_block is array (natural range <>) of unsigned(BC-1 downto 0);

end pairing_mod_package;


package body pairing_mod_package is

end pairing_mod_package;
