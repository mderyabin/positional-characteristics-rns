-- moduli set: {}

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package compare_pkg is
	
	-- moduli's count
	constant N : natural := 9;

	-- moduli bit's count
	constant pBC  : natural := 28;	
	
	constant rnsBC  : natural := 5;							-- moduli bit's count
	constant rbc	: natural := 31;						-- constants bit's count
	
	constant pairBC : natural := rbc;		-- bit's count for pairing circuit
	
	-- special type for multiply input in entity
	type rns_number is array (natural range <>) of unsigned(rnsBC-1 downto 0);
	
	-- constants
	type k_array is array (0 to N-1) of unsigned(rbc - 1 downto 0);
	constant k : k_array := (
		0 => "1000000000000000000000000000000",
		1 => "1010101010101010101010101010101",
		2 => "1100110011001100110011001100110",
		3 => "0110110110110110110110110110110",
		4 => "1010001011101000101110100010111",
		5 => "1000100111011000100111011000100",
		6 => "1101001011010010110100101101001",
		7 => "1011110010100001101011110010100",
		8 => "1101111010011011110100110111101"
	);
	
	-- moduli set for testbench
--	type m_array is array (0 to N-1) of unsigned(rnsBC downto 0);
--	constant m : m_array := (
--		"010",
--		"011",
--		"101",
--		"111"
--	);	

	-- RNS range for testbench
--	constant P : unsigned(pBC-1 downto 0) := "100100000110";
	
end compare_pkg;



package body compare_pkg is

end compare_pkg;
