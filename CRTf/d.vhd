
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.compare_pkg.all;
use work.pairing_package.all;

entity d is
	Port (
		X      : in  rns_number(0 to N-1);
		output : out unsigned (rbc-1 downto 0)
	);
end d;

architecture Behavioral of d is
	COMPONENT pairing
	GENERIC (N: NATURAL);
	PORT(
		input : IN input_block;          
		output : OUT unsigned(pairBC-1 downto 0)
	);
	END COMPONENT;
	
	component constmult_N
	Generic (
		constant k   : unsigned;
		constant N   : natural;
		constant aBC : natural;
		constant B   : natural
	);
	Port (
		a : in  unsigned;
		c : out unsigned
	);
	end component;

	signal kx : input_block(0 to N-1);
	signal q  : unsigned(pairBC - 1 downto 0);
begin

	for1 : for I in 0 to N-1 generate
--		kx(I) <= resize(X(I) * k(I), pairBC);		-- replace
		inst_constmult_N : constmult_N
		generic map (
			k => k(I),
			N => pairBC,
			aBC => X(I)'length,
			B => 5
		)
		port map (
			a => X(I),
			c => kx(I)
		);
	end generate for1;
	
	inst_pairing: pairing  
	GENERIC MAP (
		N => N
	)
	PORT MAP(
		input => kx,
		output => q
	);
	
	output <= resize(q, rbc);

end Behavioral;

