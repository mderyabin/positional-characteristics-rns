-- 
-- ���������, ���������� �� ���� ���� ����������
-- �����: ������ �������
-- ����: 24.03.2015
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


use work.pairing_package.all;

entity pairing_layer is
	Generic (
		constant N: natural := 8										-- ���������� ��������� ����
	);
	Port (
		input  : in  input_block(0 to N-1);							-- ����: ������ ������� N
		output : out input_block(0 to N/2 - 1 + (N mod 2) )	-- �����: ������ ������� N/2 (���� 1 � ������, ���� N �������)
	);
end pairing_layer;

architecture Behavioral of pairing_layer is

begin
	
	-- ��������� ������ �������� ��� ��������� �������� �������
	-- ����������� �������� �� ������ � ���������� � �����
	for1: for I in 0 to (N / 2 - 1) generate
		output(I) <= input(I) + input(N-I-1);
	end generate for1;
	
	-- ����� �������� �������� �������� � ������, ���� N �������
	if1: if (N mod 2 = 1) generate 
		output(N / 2) <= input(N / 2);
	end generate if1;

end Behavioral;

