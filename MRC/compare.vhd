--
--
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.compare_pkg.all;

entity compare is
	Port (
		X      : in  rns_number(0 to N-1);
		Y      : in  rns_number(0 to N-1);
		output : out std_logic_vector (2 downto 0)
	);
end compare;

architecture Behavioral of compare is
	component mrc
	port (
		rns : in  rns_number(0 to N-1);          
		mrc : out rns_number(0 to N-1)
	);
	end component;

	signal mrc_x : rns_number(0 to N-1);
	signal mrc_y : rns_number(0 to N-1);
begin

	inst_mrc_x: mrc
	port map (
		rns => X,
		mrc => mrc_x
	);
	
	inst_mrc_y: mrc
	port map (
		rns => Y,
		mrc => mrc_y
	);
	
	compare_pr: process (mrc_x, mrc_y)
	  variable o : std_logic_vector (2 downto 0);
	begin
		o := "010";
		for i in N-1 downto 0 loop
			if mrc_x(i) > mrc_y(i) and o = "010" then
				o    := "100";
			elsif mrc_x(i) < mrc_y(i) and o = "010" then
				o    := "001";
			end if;
		end loop;
		output <= o;
	end process compare_pr;	

end Behavioral;

