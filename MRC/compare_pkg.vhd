--
--
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package compare_pkg is

	function getAllBlocksCount(num: in natural) return natural;
	function getBlockStart(num: in natural) return natural;

    constant N      : natural := 4;
	
	constant rnsBC  : natural := 4;
	constant pairBC : natural := 2*rnsBC + 1;
	
	constant pBC    : natural := 12;
	
	type rns_number is array (natural range <>) of unsigned(rnsBC-1 downto 0);
	
	constant basisSize : natural := getAllBlocksCount(N);
    type basis_array is array (0 to basisSize-1) of unsigned(rnsBC - 1 downto 0);
	constant basis : basis_array := (
		"0100",
        "0011",
        
        "0011",
        "0110",
        "1000",
        
        "0100",
        "0100",
        "0100",
        "1000"
        
--		"0001",
--		"0010",
		
--		"0010",
--		"0001",
--		"0001",
		
--		"0011",
--		"0010",
--		"0100",
--		"0100",
		
--		"0101",
--		"0111",
--		"0110",
--		"0001",
--		"0001"--,
		
--		"00110",
--		"01000",
--		"00010",
--		"01011",
--		"00111",
--		"00011"--,
		
--		"01000",
--		"00101",
--		"01010",
--		"00100",
--		"00001",
--		"00101",
--		"01111"--,
		
--		"01001",
--		"00110",
--		"00111",
--		"10000",
--		"01100",
--		"00111",
--		"10001",
--		"10010"--,

--		"01011",
--		"01111",
--		"10010",
--		"01001",
--		"01110",
--		"01100",
--		"10010",
--		"10000",
--        "10100"
	);
	
	type m_array is array (0 to N-1) of unsigned(rnsBC - 1 downto 0);
	constant m : m_array := (
	   "0101", "0111", "1001", "1011"
		--"0010", "0011", "0101", "0111", "1011"--, "01101"--, "10001"--, "10011"--, "10111"
	);
	
	constant P : unsigned (pBC - 1 downto 0) := "110110001001";
	
end compare_pkg;



package body compare_pkg is

	function getAllBlocksCount(num: in natural) return natural is 
		variable res: natural;
	begin
		res := 0;
		if num > 1 then
			for i in 2 to num loop
				res := res + i;
			end loop;
		end if;
		return res;
	end function getAllBlocksCount;
	
	function getBlockStart(num: in natural) return natural is
	begin
		return getAllBlocksCount(num-1);
	end function getBlockStart;

end compare_pkg;
