library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- calculate c = k*a
entity constmult is
	Generic (
		constant k   : unsigned := "11110111111101111111011111110111";	-- constant
		constant BC  : natural  := 32;											-- size of a
		constant cBC : natural  := 64;											-- size of output
		constant B   : natural  := 8												-- size of block's
	);
	Port (
		a : in  unsigned(BC-1 downto 0);					-- non-constant input
		c : out unsigned(cBC - 1 downto 0)	-- = a * k
	);
end constmult;

architecture Behavioral of constmult is

	-- components declaration ----

	------------------------------
		
	-- pairint functions declaration ----
	-- ���������� ��������� �� ����� ���� � ������� layer_num
	function LayerInputSize (layer_num: natural; digit_count: natural) return natural is
		variable result : natural;
	begin
		result := digit_count;
		for i in 2 to layer_num loop
			result := (result / 2) + (result mod 2);
		end loop;
		return result;
	end LayerInputSize;
	
	-- ��������� ������� ��������� ���� layer_num � � ����� ������� ��������
	function LayerStart (layer_num: natural; digit_count: natural) return natural is
		variable result : natural;
	begin
		result := 0;
		for i in 1 to layer_num-1 loop
			result := result + LayerInputSize(i, digit_count);
		end loop;
		return result;
	end LayerStart;
	
	-- ���������� ����� - ����������� �������� �� ���������� ��������� ������� ���� (���������)
	-- � ����������� �����
	function LayersCount(digit_count: in natural) return natural is
		variable res : natural;
		variable vn  : unsigned(31 downto 0);	-- �����, ����������� ������� ���� natural
	begin
		res := 0;
		if digit_count = 0 then
		  return res;
		end if;
		
		vn := to_unsigned(digit_count - 1, 32);		-- digit_count-1 ����� ����� ������� log(2^t)
		
		-- ������� ���������� ��� � ����� vn
		while to_integer(vn) /= 0 loop
			res := res + 1;
			vn := vn srl 1;
		end loop;
		
		return res;
	end function LayersCount;
	
	-- ����� ���������� ��������� �� ���� �����, ������� ��������
	function AllLayersSize(layers_count, digit_count : in natural) return natural is
		variable res : natural;
	begin
		res := 0;
		
		-- ������������ ���������� �������� �� ���� ����� ���� �������� �������
		for ln in 1 to layers_count+1 loop
			res := res + LayerInputSize(ln, digit_count);
		end loop;
		
		return res;
	end function AllLayersSize;
	------------------------------	
	
	-- constants declaration -----
	constant k_bc        : natural := k'length;
	constant pow2_B      : natural := to_integer(SHIFT_LEFT(to_unsigned(1, B+1), B));
	 
	constant bl_count    : natural := BC / B;
	constant add_B       : natural := BC - B * bl_count;
	
	constant pow2_add_B  : natural := to_integer(SHIFT_LEFT(to_unsigned(1, add_B+1), add_B));
	
	constant l_count     : natural := LayersCount(bl_count);					-- ���������� �����
	
	constant all_l_count : natural := AllLayersSize(l_count, bl_count);	-- ���������� ��������� �� ���� �����
	------------------------------
		
	-- types declaration ---------
	type RAM_type is array (0 to pow2_B - 1) of unsigned (k_bc + B - 1 downto 0);
	
	type add_RAM_type is array (0 to pow2_add_B - 1) of unsigned (k_bc + add_B - 1 downto 0);
	
	type bl_type is array (0 to all_l_count - 1) of unsigned (k_bc + BC - 1 downto 0);
	------------------------------
	
	-- functions declaration -----
--	function min (x: natural; y : natural) return natural is
--	begin
--		if (x > y) then return y; end if;
--		return x;
--	end min;
	
	function CreateMultRAM return RAM_type is
		variable result : RAM_type;
	begin
		for i in 0 to pow2_B - 1 loop 
			result(i) := to_unsigned(i, B) * k;
		end loop;
		return result;
	end CreateMultRAM;
	
	function CreateMultAddRAM return add_RAM_type is
		variable result : add_RAM_type;
	begin
		if add_B /= 0 then
			for i in 0 to pow2_add_B - 1 loop 
				result(i) := to_unsigned(i, add_B) * k;
			end loop;
		else
			for i in 0 to pow2_add_B - 1 loop 
				result(i) := (others => '0');
			end loop;
		end if;
		return result;
	end CreateMultAddRAM;
	
	
	------------------------------

	-- RAM declaration -----------
	constant mult_RAM : RAM_type := CreateMultRAM;
	constant mult_add_RAM : add_RAM_type := CreateMultAddRAM;
	------------------------------
	
	-- signals declaration -------
	signal blocks    : bl_type;
	signal add_block : unsigned (k_bc + BC - 1 downto 0);
	
	
	signal last_block : unsigned (k_bc + BC - 1 downto 0);
	signal c1 : unsigned (k_bc + BC - 1 downto 0);
	------------------------------
	
begin

	level_1: for i in 0 to bl_count-1 generate
	begin
		blocks(i) <= SHIFT_LEFT(resize(mult_RAM(to_integer(a(B*(i+1)-1 downto B*i))), BC + k_bc), i * B); 
	end generate;
	
	if0 : if bl_count /= 0 generate
		layers: for i in 1 to l_count generate
		begin
			
			-- ������� 2: �������� ��������� �������� �����������
			level_2: for j in 0 to LayerInputSize(i, bl_count) / 2 - 1  generate
			begin
				blocks(LayerStart(i + 1, bl_count) + j) <= blocks(LayerStart(i, bl_count) + j) + blocks(LayerStart(i, bl_count) + LayerInputSize(i, bl_count) - 1 - j);
			end generate level_2;
			
			-- ������� ������������ �������� � ������ ��������� ����� �������� �� ����
			if_odd: if ((LayerInputSize(i, bl_count) mod 2) = 1) generate
				blocks(LayerStart(i+1, bl_count) + LayerInputSize(i+1, bl_count)-1) <= blocks(LayerStart(i, bl_count) + LayerInputSize(i, bl_count)/2);
			end generate if_odd;
			
		end generate layers;
		
		last_block <= blocks(all_l_count - 1);
	end generate if0;
	
	if1 : if bl_count = 0 generate
		last_block <= (others => '0');
	end generate if1;
	
	if_over: if add_B /= 0 generate
		add_block <= SHIFT_LEFT(resize(mult_RAM(to_integer(a(BC-1 downto bl_count * B))), BC + k_bc), bl_count * B);
		
		c1 <= last_block + add_block;
	end generate if_over;
	
	if_n_over: if add_B = 0 generate
		c1 <= last_block;
	end generate if_n_over;

	c <= resize(c1, cBC);

end Behavioral;

