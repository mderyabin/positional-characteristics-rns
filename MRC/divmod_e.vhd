
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.compare_pkg.all;

entity divmod_e is
	Generic (
		constant I  : natural
	);
	Port (
		num    : in  unsigned(pairBC-1 downto 0);
		quot   : out unsigned(rnsBC      downto 0);
		remain : out unsigned(rnsBC-1    downto 0)
	);
end divmod_e;

architecture Behavioral of divmod_e is

	function getTopBit(x: unsigned) return integer is 
	begin
	 for J in x'RANGE loop
		if x(J)='1' then
		  return J;
		end if;
	 end loop;
	 return -1;
	end function getTopBit;
	
	type topbits_array is array (0 to N-1) of integer;
	
	function getTopBitArray return topbits_array is
		variable result : topbits_array;
	begin
		for i in 0 to N-1 loop
			result(i) := getTopBit(m(I));
		end loop;
		return result;
	end;
	
	constant topbits : topbits_array := getTopBitArray;

	
	-- this internal procedure computes UNSIGNED division
	-- giving the quotient and remainder.
	procedure DIVMOD (signal NUM : unsigned; signal XQUOT, XREMAIN: out UNSIGNED) is
	 variable TEMP: UNSIGNED(NUM'LENGTH downto 0);
	 variable QUOT: UNSIGNED(NUM'LENGTH-1 downto 0);
	 --alias DENOM: UNSIGNED(m(I)'LENGTH-1 downto 0) is m(I);
	 --variable TOPBIT: INTEGER;
	begin
	 TEMP := "0"&NUM;
	 QUOT := (others => '0');
	
	 assert topbits(I) >= 0 report "DIV, MOD, or REM by zero" severity ERROR;

	 for J in NUM'LENGTH-(topbits(I)+1) downto 0 loop
		if TEMP(topbits(I)+J+1 downto J) >= "0"&m(I)(topbits(I) downto 0) then
		  TEMP(topbits(I)+J+1 downto J) := (TEMP(topbits(I)+J+1 downto J))
				-("0"&m(I)(topbits(I) downto 0));
		  QUOT(J) := '1';
		end if;
		assert TEMP(topbits(I)+J+1)='0'
			 report "internal error in the division algorithm"
			 severity ERROR;
	 end loop;
	 XQUOT <= RESIZE(QUOT, XQUOT'LENGTH);
	 XREMAIN <= RESIZE(TEMP, XREMAIN'LENGTH);
	end DIVMOD;

begin

	DIVMOD(num, quot, remain);

end Behavioral;

