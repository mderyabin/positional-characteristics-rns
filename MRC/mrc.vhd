---
---
---

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.compare_pkg.all;
use work.pairing_package.all;

entity mrc is
	Port (
		rns : in  rns_number(0 to N-1);
		mrc : out rns_number(0 to N-1)
	);
end mrc;

architecture Behavioral of mrc is
	COMPONENT pairing
	GENERIC (N: NATURAL);
	PORT(
		input  : IN  input_block;          
		output : OUT unsigned(pairBC-1 downto 0)
	);
	END COMPONENT;
	
	COMPONENT divmod_e
	GENERIC (
		constant I : natural
	);
	PORT(
		num    : in  unsigned(pairBC-1 downto 0);
		quot   : out unsigned(rnsBC  	 downto 0);
		remain : out unsigned(rnsBC-1  downto 0)
	);
	END COMPONENT;
	
    component constmult
    Generic (
        constant k   : unsigned;
        constant BC  : natural;
        constant cBC : natural;
        constant B   : natural
    );
    Port (
        a : in  unsigned;
        c : out unsigned
    );
    end component;
    
    COMPONENT Mod_p
     GENERIC (
        constant p    : unsigned;
        constant B    : natural;
        constant X_bc : natural
     );
    PORT(
         X       : IN   unsigned;
         X_mod_p : OUT  unsigned
        );
    END COMPONENT;

	signal signals: input_block(0 to basisSize-1);


	type sums_array is array (0 to N-2) of unsigned (pairBC -1 downto 0);
	signal sums: sums_array;
	
	signal sums1: sums_array;
	
	type t_array is array (2 to N-1) of unsigned (rnsBC downto 0);
	signal t: t_array;
	
	
begin

	mrc(0) <= rns(0);
	
	for1: for I in 2 to N generate
		for2 : for J in 0 to I-1 generate
		  inst_constmult1: constmult
            generic map (
                k => basis(getBlockStart(I) + J),
                BC => rnsBC+1,
                cBC => pairBC,
                B => 5
            )
            port map (
                a => '0'&rns(J),
                c => signals(getBlockStart(I) + J)
            );
			
			--signals(getBlockStart(I) + J) <= '0'&rns(J) * basis(getBlockStart(I) + J);
		end generate for2;
	end generate for1;
		
	for1_1: for I in 2 to N generate
		inst_pairing: pairing
		generic map (
			N => I
		)
		port map (
			input => signals(getBlockStart(I) to getBlockStart(I+1)-1),
			output => sums(I-2)
		);	
	end generate for1_1;
	
	
	sums1(0) <= sums(0);
	
	for4: for I in 2 to N-1 generate
		
		inst_divmod: divmod_e 
		generic map (
			I => I-1
		)
		port map (
			num    => sums1(I-2),
			quot   => t(I),
			remain => mrc(I-1)
		);
		
		sums1(I-1) <= sums(I-1) + t(I);
	end generate for4;

	-- �������� �� Mod_p
	--mrc(N-1) <= sums1(N-2) mod m(N-1);
	inst_mod: Mod_p 
    GENERIC MAP (
        p => m(N-1),
        B => 4,
        X_bc => pairBC
    )
    PORT MAP (
      X => sums1(N-2),
      X_mod_p => mrc(N-1)
   );

end Behavioral;

